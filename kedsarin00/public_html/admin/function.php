<?php
	@session_start();
	set_time_limit(0);
	date_default_timezone_set("Asia/Bangkok");
	$server = "localhost";			// ชื่อของ server ซึ่งมันจะเป็น localhost
	$username = "kedsarin";				// ชื่อ username ที่ใช้ในการติดต่อกับฐานข้อมูล
	$password = "234207";			// รหัสผ่านในการติดต่อกับฐานข้อมูล
	$database = "kedsarin";		// ชื่อฐานข้อมูลที่จะใช้ในการติดต่อ
	$security = 1;					// ค่าความปลอดภัยของระบบ
	$thaiMonth=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
	$thaiMonth2=array("ม.ค","ก.พ","มี.ค","เม.ย","พ.ค","มิ.ย","ก.ค","ส.ค","ก.ย","ต.ค","พ.ย","ธ.ค");
	$thaiMonthShort=array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
	$thaiDate=array("วันอาทิตย์","วันจันทร์","วันอังคาร","วันพุธ","วันพฤหัสบดี","วันศุกร์","วันเสาร์");
	
	// ที่ใช้ mysql_pconnect($server,$username,$password,$fag) เพื่อไม่ต้องมีการใช้ mysql_close() อีก
	$link=mysqli_connect($server,$username,$password);
	$db=mysqli_select_db($link,$database);
	query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");

	//ค่าเริ่มต้นโดยไม่ให้แสดงผลของ error massage แต่ให้ควบคุมผ่านโปรแกรมเอง
	$error_message=1;	// ตัวแปรนี้เอาไว้ควบคุมการแสดงผล error massage จากฟังก์ชัน getList() และ query()

	function connect_database_server($branch){
		global $link;
		global $host_to_connect;
		global $username_array;
		global $password_array;
		$i=0;
		while(!empty($host_to_connect[$branch][$i])){
			$link=@mysql_pconnect($host_to_connect[$branch][$i],$username_array[$branch],$password_array[$branch]);
			if($link){
				query("SET character_set_results = 'utf8', character_set_client = 'utf8', character_set_connection = 'utf8', character_set_database = 'utf8', character_set_server = 'utf8'");
				break;
			} else {
				$i++;
			}
		}
	}
	//ฟังก์ชันสำหรับการเปลี่ยนค่าอายุของพนักงานแต่ละคน เมื่อมีการตรวจสอบข้อมูล
	//วิธีการคือ นำเดือนที่เกิดมาเปรียบเทียบกับเดือนปัจจุบัน ถ้าเดือนที่เกิด มากกว่าเดือนปัจจุบัน จะนำผลต่างของปีมาลบด้วย 1 ปี แต่ถ้าปีที่เกิดน้อยกว่าหรือเท่ากับ จะใช้ค่าผลต่างของปี
	function updateAge($empID){
		$tmp=getList("SELECT dateBirth FROM people WHERE peopleID = '$empID'");
		$id=explode("-",$tmp[0][dateBirth]);
		if($id[1]>date("m")){
			//กรณีที่ยังไม่ถึงเดือนเกิด
			$empAge=date("Y")-$id[0]-1;
		}else{
			//ผ่านเดือนเกิดมาแล้ว
			$empAge=date("Y")-$id[0];
		}
		query("UPDATE people SET age = '$empAge' WHERE peopleID = '$empID'");
	}
	//ฟังก์ชันนี้เป็นฟังก์ชันที่รับค่า input มาในรูปแบบ %ข้อความ% โดยจะทำการตัดเครื่องหมาย % ออกในตัวแรกและตัวสุดท้าย จากนั้นคำตอบที่จะเป็น ข้อความ
	function splitPercent($str){
		$ret=str_split($str);
		$num=sizeof($ret);
		unset($ret[0]);
		unset($ret[$num-1]);
		$ret=implode("",$ret);
		return $ret;
	}
	function get_web_page_control($db,$sql,$index,$value){
		query("USE ".$db);
		$getVariable=getList($sql);
		for($i=0;$i<sizeof($getVariable);$i++){
			$VARIABLE[$getVariable[$i][$index]]=$getVariable[$i][$value];
		}
		return $VARIABLE;
	}
	function getList($sql=""){
		global $link;
		global $error_message;
		$db_query=mysqli_query($link,$sql);
		if(!empty($db_query)){
			while($tmp=mysqli_fetch_assoc($db_query)){
				$result[]=$tmp;
			}
			if(isset($db_query)){
				return $result;
			}
		}
	}
	function shrink2fitCell($word,$width,$fontSize){
		$amountChar=countAmountLetter($word,"THAILAND");
		while($amountChar > (7.5*$width/$fontSize)){
		   $fontSize --;
		   if($fontSize == 1){
				break;
		   }
		}
		 print("<font style=\"font-size:".$fontSize."px;\">".$word."</font>");
	}

	/*เป็นฟังก์ชันที่ใช้สำหรับการดำเนินการกับฐานข้อมูลโดยไม่มีการคืนค่ากลับ เช่นคำสั่ง UPDATE INSERT DELETE ALTER เป็นต้น*/
	function query($sql=""){
		global $link;
		global $error_message;
		global $database;
		list($cm,$db)=explode(" ",$sql);
		if($cm=="USE"){$database=$db;}
		$t = mysqli_query($link,$sql);
		if($t){
			return 1;
		} else {
			return 0;
		}
	}
	function feedError($errno,$errstr){
		global $error_message;
		$func=explode(":",$errstr);
		if($error_message){
			print("<table border=\"1\" width=\"100%\" bgcolor=\"pink\" bordercolor=\"red\">\n");
			print("\t<tr>\n");
			print("\t\t<td>\n");
			print("\t\t\t<font color=\"red\" face=\"system\"><b>Feeding error.</b></font>\n<br />\n");
			print("\t\t\t<font face=\"system\">Feeding Operation from function ".$func[0].".</font><br />\n");
			print("\t\t\t<font face=\"system\">Because");
			for($i=1;$i<sizeof($func);$i++){
				print($func[$i]." ");
			}
			print(".</font><br />\n");
			print("\t\t</td>\n");
			print("\t</tr>");
			print("</table>");
		}
	}

	/* ฟังก์ชัน feedFromCSV เป็นฟังก์ชันที่ใช้สำหรับการอ่านแฟ้มข้อมูลซึ่งมีนามสกุลเป็น CSV หรือแฟ้มที่มีลักษณะเหมือนกับแฟ้ม .CSV*/
	function feedFromCSV($file="",$delemiter=","){
		set_error_handler("feedError");
		$file=fopen($file,"r");
		while(!feof($file)){
			$data[]=explode($delemiter,rtrim(fgets($file)));
		}
		return $data;
	}
	
	function data_in_array($word,$array){
		for($i=0;$i<sizeof($array);$i++){
			if($word==$array[$i]){
				return 1;
			}
		}
		return 0;
	}

	function tis2utf8($tis) {
	for( $i=0 ; $i< strlen($tis) ; $i++ ){
		$s = substr($tis, $i, 1);
		$val = ord($s);
		if( $val < 0x80 ){
			$utf8 .= $s;
		} elseif ( ( 0xA1 <= $val and $val <= 0xDA ) or ( 0xDF <= $val and $val <= 0xFB ) ){
			$unicode = 0x0E00 + $val - 0xA0;
			$utf8 .= chr( 0xE0 | ($unicode >> 12) );
			$utf8 .= chr( 0x80 | (($unicode >> 6) & 0x3F) );
			$utf8 .= chr( 0x80 | ($unicode & 0x3F) );
		}
	}
		return $utf8;
	}

	function printThaiDate($date){
		list($y,$m,$d)=explode("-",$date);
		$y+=543;
		$tm=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		return (int)$d." ".$tm[$m-1]." พ.ศ. ".$y;
	}

	function printShotEngDate($date){
		list($y,$m,$d)=explode("-",$date);
		$tm=array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
		return (int)$d."-".$tm[$m-1]."-".$y;
	}
	
	function printThaiMonthYear($date){
		list($y,$m,$d)=explode("-",$date);
		$y+=543;
		$tm=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		return $tm[$m-1]." ".$y;
	}
	
	function printShortThaiDate($date){
		list($y,$m,$d)=explode("-",$date);
		$y+=543;
		$tm=array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		return (int)$d." ".$tm[$m-1]." ".$y;
	}
	
	function printShortThaiDateAndYear($date){
		list($y,$m,$d)=explode("-",$date);
		$y+=543;
		$tm=array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		return (int)$d." ".$tm[$m-1]." ".substr($y,2,2);
	}

	function printShortSlateThaiDate($date){
		list($y,$m,$d)=explode("-",$date);
		$y+=543;
		$tm=array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		return (int)$d."/".$m."/".substr($y,2,2);
	}
	function printlongSlateThaiDate($date){
		list($y,$m,$d)=explode("-",$date);
		$y+=543;
		$tm=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		return (int)$d." ".$tm[$m-1]." ".$y;
	}

	function printShortDate($date){
		list($y,$m,$d) = explode("-",$date);
		$y+=543;
		$tm=array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		return (int)$d."/".$m."/".substr($y,2,2);
	}

	function printShortDate2($date){
		list($y,$m,$d)=explode("-",$date);
		$y += 543;
		$tm = array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		return $d."/".$m."/".substr($y,2,2);
	}
	//-----grace Program store------
	function printdateSQL($date){
		list($d,$m,$y) = explode("/",$date);
		$y-=543;
		return $y."-".$m."-".$d;
	}
	function printShortNumDate($date){
		list($y,$m,$d)=explode("-",$date);
		$y+=543;
		return $d."/".$m."/".substr($y,2,2);
	}
	
	//----------------
	function KAK(){
		$get_GET="";
		$get_POST="";
		$get_FILE="";
		$get_SESSION="";
		$timeStamp=date("Y-m-d H:i:s",mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y")));
		foreach ($GLOBALS['_POST'] as $key => $value){
					$get_POST.="[[".$key."]=>".$value."]";
				}
		foreach ($GLOBALS['_GET'] as $key => $value){
					$get_GET.="[[".$key."]=>".$value."]";
				}
		foreach ($GLOBALS['_FILES'] as $key => $value){
					$get_FILE.="[[".$key."]=>".$value."]";
				}
		foreach ($GLOBALS['_SESSION'] as $key => $value){
					$get_SESSION.="[[".$key."]=>".$value."]";
				}
		if(strlen($get_POST) <= 4096 AND strlen($get_GET) <= 4096 AND strlen($get_FILE) <= 4096 AND strlen($get_SESSION) <= 4096){
			query_transaction("USE security","INSERT INTO security VALUES (NULL,'$_SERVER[REMOTE_ADDR]','$_SERVER[SERVER_NAME]','$_SERVER[REQUEST_URI]','$get_POST','$get_GET','$get_FILE','$get_SESSION','$GLOBALS[server]','$GLOBALS[database]','$timeStamp')");
		} else {
			$file=fopen("../include/security/".date("Y-m-d").".csv","a");
			fputs($file,$_SERVER[REMOTE_ADDR].",".$_SERVER[SERVER_NAME].",".$_SERVER[REQUEST_URI].",".$get_POST.",".$get_GET.",".$get_FILE.",".$get_SESSION.",".$GLOBALS[server].",".$GLOBALS[database].",".$timeStamp);
			fclose($file);
			$strFile="../include/security/".date("Y-m-d",mktime(0,0,0,date("m")-3,date("d"),date("Y"))).".csv";
			if(file_exists($strFile)){
				unlink($strFile);
			}
		}
	}

	function query_transaction(){
		global $link;
		$bool=0;
		$num=0;
		$sql=func_get_args();
		mysql_query("BEGIN");
		for($i=0;$i<sizeof($sql);$i++){
			if($sql[$i] != ""){
				$how=query($sql[$i]);
				$bool+=$how;
				$num++;
			}
		}
		if($bool==$num){
			mysql_query("COMMIT");
			return 1;
		} else {
			mysql_query("ROLLBACK");
			return 0;
		}
	}

	function getHoliday($CompID = 1){
		query("USE hr");
		$tmp=getList("SELECT * FROM holiday WHERE YEAR(holidayDate) IN ('".(date("Y")+1)."','".date("Y")."','".(date("Y")-1)."') AND holidayCompID = '".$CompID."'");
		for($i=0;$i<sizeof($tmp);$i++){
			$holiday[]=$tmp[$i][holidayDate];
		}
		return $holiday;
	}
	
	function getVacationDate($CompID = 1){
		switch($CompID){
			case 1	:	return array(0);
						break;
			case 2	:	return array(0,6);
						break;
		}
	}
	function str_split_unicode($str, $l = 0) {
		if ($l > 0) {
			$ret = array();
			$len = mb_strlen($str, "UTF-8");
			for ($i = 0; $i < $len; $i += $l) {
				$ret[] = mb_substr($str, $i, $l, "UTF-8");
			}
			return $ret;
		}
		return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
	}
	function countThai($arr){
		$thaichar=array('ก','ข','ค','ง','จ','ฉ','ช','ซ','ฌ','ญ','ฐ','ฑ','ฒ','ณ','ด','ต','ถ','ท','ธ','น','บ','ป','ผ','ฝ','พ','ฟ','ภ','ม','ย','ร','ล','ว','ศ','ษ','ส','ห','ฬ','อ','ฮ','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
		$thaivowel=array('ะ','า','ำ','เ','แ','โ','ใ','ไ','ๅ','ๆ',' ','ฯ','"','(',')','-','/',',','.','+','=','0','1','2','3','4','5','6','7','8','9');
		$allChar=array_merge($thaichar,$thaivowel);
		$count=0;
		for($i=0;$i<sizeof($arr);$i++){
			if(data_in_array($arr[$i],$allChar)){
				$count++;
			}
		}
		return $count;
	}
	function countAmountLetter($str,$type="ENGLISH"){
		switch($type){
			case "ENGLISH"	:	return count(str_split_unicode($str));
								break;
			case "THAILAND"	:	return countThai(str_split_unicode($str));
								break;
		}
	}
	function amountLine($str,$amountPerLine){
		return ceil($str/$amountPerLine);
	}
	function generateSelection($sql,$db2Query,$idAttribute,$fieldValue,$fieldName,$firstOption="กรุณาเลือก",$style="",$formSubmit=0,$selected=1,$varCheck="",$extendEvent=""){
		print("<select id=\"".$idAttribute."\" name=\"".$idAttribute."\"".($formSubmit==1 ? "onchange=\"this.form.submit();\"" : "")." style=\"".$style."\" ".$extendEvent.">");
		print("<option value=\"-\" >".$firstOption."</option>");
		
		//connect_database_server(''.$server.'');
		query("USE ".$db2Query);
		$getDataInoption=getList($sql);
		for($indexToShow=0;$indexToShow<sizeof($getDataInoption);$indexToShow++){
			$commandSelected="";
			if($selected==1){
				$commandSelected = $varCheck == $getDataInoption[$indexToShow][$fieldValue] ? "selected=\"selected\"" : "";
			}
			print("<option value=\"".$getDataInoption[$indexToShow][$fieldValue]."\" ".$commandSelected." title=\"".$getDataInoption[$indexToShow][$fieldName]."\">".$getDataInoption[$indexToShow][$fieldName]."</option>");
		}
		print("</select>");
	}

	function selectManpower($set_select,$type){
		  query("USE productionnew");
		  $sql    = "SELECT * FROM plan_major_total_production WHERE PMTPDetail <> '' ''";
		  $result   = mysql_query($sql);
		  $count_row  = mysql_num_rows($result);
		  if($count_row>0){//if1
		    $combo  = "";
		    while($row = mysql_fetch_array($result)){//while
		      $PMTPID   = $row['PMTPID'];     
		      $PMTPDetail   = $row['PMTPDetail'];
		      if($set_select==$PMTPID){ $check = "selected"; } else{ $check = ""; }
		      if($type=='jump'){ $PMTPID = $PMTPID; }
		      $combo  .= "<option value='".$PMTPID."'' ".$check.">".$PMTPDetail."</option>";
		   }//while
		  }//if1
		return $combo;
	}
	function SearchDateManpower($startDate, $endDate){
		$startDate = strtotime($startDate);
		$endDate = strtotime($endDate);
		$ArrayDate = array();
		while($startDate <= $endDate){
			$cyberdate = date('Y-m-d', $startDate);
			$ArrayDate[] = $cyberdate.'<br>';
			$startDate += (1 * 24 * 3600); // add 7 days
	}
	 return($ArrayDate);
}

	function printCurrency($money=0,$showSatang=1,$amountDecimalPlace=2){
		$negativeNumber=$money>=0 ? "" : "-";
		list($money,$satang)=explode(".",abs(sprintf("%.".$amountDecimalPlace."f",$money)));
		if(empty($satang) OR $satang==0){
			$satang="00";
		} else {
			$satang=str_pad($satang,$amountDecimalPlace,0,STR_PAD_RIGHT);
			$tmpSatang=str_split($satang);
			$tmpSatang[$amountDecimalPlace-1] += $tmpSatang[$amountDecimalPlace] >= 5 ? 1 : 0 ;
			$satang=substr(implode("",$tmpSatang),0,$amountDecimalPlace);
		}

		$money=str_split($money);
		$money=array_reverse($money);
		for($i=0;$i<sizeof($money);$i++){
			$currency[]=$money[$i];
			if($i%3==2 AND ($i!=sizeof($money)-1)){
				$currency[]=",";
			}
		}
		$currency=array_reverse($currency);
		$currency=implode("",$currency);
		if($showSatang){
			$currency.=".".$satang;
		}
		return $negativeNumber."".$currency;
	}

	function date2sec($date){
		list($y,$m,$d)=explode("-",$date);
		return mktime(0,0,0,$m,$d,$y);
	}
	
	function datetime2sec($datetime){
		list($date,$time)=explode(" ",$datetime);
		list($y,$m,$d)=explode("-",$date);
		list($H,$i,$s)=explode(":",$time);
		return mktime($H,$i,$s,$m,$d,$y);
	}

	function getOT(){
	//แปลงฐานข้อมูล MS Acces เพื่อหา OT พนักงาน
		query("USE hr");
		$getLastUpdate=getList("SELECT * FROM attendance_update WHERE attType = 'OT'");
		list($year,$month,$date)=explode("-",$getLastUpdate[0][lastLog]);
		$idExcept=array(46108,46109,46203,46206,46207,46208);
		$dateProcess=$getLastUpdate[0][lastLog];
		$i=0;
		$order=0;
		while($dateProcess!=date("Y-m-d",mktime(0,0,0,date("m"),date("d")-1,date("Y")))){
			$dateProcess=date("Y-m-d",mktime(0,0,0,$month,$date+1,$year));
			list($year,$month,$date)=explode("-",$dateProcess);
			//$getCountOfNormalOT=getListODBC("att18","SELECT CHECKINOUT.SENSORID,CHECKINOUT.CHECKTIME,USERINFO.BADGENUMBER FROM USERINFO,CHECKINOUT WHERE USERINFO.USERID=CHECKINOUT.USERID AND DAY(CHECKINOUT.CHECKTIME)=$date AND MONTH(CHECKINOUT.CHECKTIME)=$month AND YEAR(CHECKINOUT.CHECKTIME)=$year AND HOUR(CHECKINOUT.CHECKTIME) BETWEEN 17 AND 18 AND MINUTE(CHECKINOUT.CHECKTIME)>=10 ORDER BY BADGENUMBER ASC");
			$getScanPerson=getListODBC("att18","SELECT DISTINCT USERINFO.BADGENUMBER FROM USERINFO,CHECKINOUT WHERE USERINFO.USERID=CHECKINOUT.USERID AND DAY(CHECKINOUT.CHECKTIME)=$date AND MONTH(CHECKINOUT.CHECKTIME)=$month AND YEAR(CHECKINOUT.CHECKTIME)=$year ORDER BY BADGENUMBER ASC");
			for($i=0;$i<sizeof($getScanPerson);$i++){
				$id=$getScanPerson[$i][BADGENUMBER];
				if(!data_in_array($id,$idExcept)){
					$getDataScanPerson=getListODBC("att18","SELECT CHECKTIME,SENSORID FROM USERINFO,CHECKINOUT WHERE USERINFO.USERID=CHECKINOUT.USERID AND DAY(CHECKINOUT.CHECKTIME)=$date AND MONTH(CHECKINOUT.CHECKTIME)=$month AND YEAR(CHECKINOUT.CHECKTIME)=$year AND USERINFO.BADGENUMBER LIKE $id");
					//กรณีที่มีการ scan ปกติจะพบว่าข้อมูลในแต่ละวันจะต้องมีการ scan นิ้ว 4 ครั้งต่อวันและในกรณีทำ ot จะต้องมีข้อมูลใน array ตัวที่ 5 โดยในส่วนนี้จะพบปัญหาตรงที่หากเราไม่ได้แสกนนิ้วเข้าออกตามปกติ จึงจะไม่ใช้การดึงข้อมูลแบบนี้
					/*if(!empty($getDataScanPerson[4])){
						list($attDate,$attTime)=explode(" ",$getDataScanPerson[4][CHECKTIME]);
						$sensor=$getDataScanPerson[4][SENSORID];
						//query("INSERT INTO attendancelog VALUES (NULL,'$id','$attDate','$attTime','$sensor','C')");
						$order++;
						print("COMPLETED ".$order." QUERY<br />\n");
					}*/
					//ส่วนนี้จะเป็นการ scan นิ้วออกในช่วงทำ ot เพราะด้วยนิสัยมนุษย์มักจะไม่มีทำงานเกินกว่าเวลากำหนดอยู่แล้ว ฉะนั้นจึงใช้วิธีการตรวจสอบที่ว่า array ตัวสุดท้ายของวันนั้นว่ามีค่าเป็น 21.xx.xx หรือไม่ซึ่งถ้าใข้ก็คือการ scan นิ้วออกตอนทำ OT
					list($attDate,$attTime)=explode(" ",$getDataScanPerson[sizeof($getDataScanPerson)-1][CHECKTIME]);
					$tmp=explode(":",$attTime);
					if($tmp[0] > 19){
						if(date("w",mktime(0,0,0,$month,$date,$year))==0){
							query_transaction("USE hr","INSERT INTO attendancelog VALUES (NULL,'$id','$attDate','$attTime',NULL,'E')");
						} else {
							query_transaction("USE hr","INSERT INTO attendancelog VALUES (NULL,'$id','$attDate','$attTime',NULL,'C')");
						}
						$order++;
					}
				}
			}
			$i++;
		}
		if($order!=0){		
			query_transaction("USE hr","UPDATE attendance_update SET lastLog='".date("Y-m-d")."' , lastTimeLog='".date("H:i:s")."' WHERE attType = 'OT'");
		}
	}

	function getListODBC($driver,$sql="") {
		$conn=odbc_pconnect($driver,"","");
		$rs=odbc_exec($conn,$sql);
		$i=0;
		while($tmp=odbc_fetch_array($rs)){
			foreach ($tmp as $key => $value){
				$result[$i][tis2utf8($key)]=tis2utf8($value);
			}
			$i++;
		}
		odbc_close($conn);
		odbc_close_all();
		return $result;
	}

	function althoughPrintEmpty($str,$valueWhenStrNULL="&nbsp;"){
		if(is_numeric($str)){
			list($decimal,$precision)=explode(".",$str);
			if($precision == 0){
				$str = $decimal;
			}
		}
		return (empty($str) OR $str == "0  543") ? $valueWhenStrNULL : $str;
	}

	function getWidthTableReport($sizepaper="A4",$pageOption="portrait"){
		switch($sizepaper){
			case "A4"	:	switch($pageOption){
								case "portrait"	:
								case 1			:
													return 180;
													break;
								case "landscape"	:
								case 2			:
													return 280;
													break;
							}
							break;
			case "A3"	:	switch($pageOption){
								case "portrait"	:
								case 1			:
													return 280;
													break;
								case "landscape"	:
								case 2			:
													return 400;
													break;
							}
							break;
		}
	}
	
	function isHoliday($dateInput){
		$holiday=getHoliday();
		if(date("w",date2sec($dateInput)) == 0 OR data_in_array($dateInput,$holiday)){
			return 1;
		} else {
			return 0;
		}
	}

	function dateDiff($dateFinish,$dateStart){
		return ((date2sec($dateFinish)-date2sec($dateStart))/60/60/24);
	}

	function dateDiffWorker($dateFinish,$dateStart){
		$holiday=getHoliday();
		$numDateWorker=dateDiff($dateFinish,$dateStart);
		$numDateNonWorker=0;
		for($i=0;$i<=$numDateWorker;$i++){
			if(date("w",date2sec($dateStart)+24*60*60*$i) == 0 OR data_in_array(date("Y-m-d",date2sec($dateStart)+24*60*60*$i),$holiday)){
				$numDateNonWorker++;
			}
		}
		return $numDateWorker-$numDateNonWorker;
	}

	function getEmpCodeFromSession(){
		global $database;
		query("USE hr");
		$getEmpCode=getList("SELECT empCode FROM employee WHERE empID = '".$_SESSION[sess_empNo]."'");
		return $getEmpCode[0][empCode];
	}
	

	function getArrayDepCodeCommandee($depCode){
		$getTotalDepCode=getDepCodeCommandee($depCode);
		$depCodeArray=explode(",",$getTotalDepCode);
		unset($depCodeArray[sizeof($posCodeArray)-1]);
		return $depCodeArray;
	}

	function getPosCodeCommandee($posCode){
		global $database;
		query("USE hr");
		$getEmpCode=getList("SELECT posCode FROM position WHERE posCommander = '".$posCode."' AND isUsage = '1'");
		$commandee="";
		for($i=0;$i<sizeof($getEmpCode);$i++){
			$commandee.=$getEmpCode[$i][posCode].",";
			$tmp=getposCodeCommandee($getEmpCode[$i][posCode]);
			if(!empty($tmp)){
				$commandee.=$tmp;
			}
		}
		return $commandee;
	}
	function getPosCodeCommandeeElective($empCode){
		global $database;
		query("USE hr");
		$getEmpCodeElective=getlist("Select * from elective_position where EPEmpCode = '$empCode' and EPIsUsage = '1'");
		$commandee="";
		for($i=0;$i<sizeof($getEmpCodeElective);$i++){
			$getEmpCode=getList("SELECT posCode FROM position WHERE posCommander = '".$getEmpCodeElective[$i][EPPosCode]."' AND isUsage = '1'");
			for($j=0;$j<sizeof($getEmpCode);$j++){
				$commandee.=$getEmpCode[$i][posCode].",";
				$tmp=getposCodeCommandee($getEmpCode[$i][posCode]);
				if(!empty($tmp)){
					$commandee.=$tmp;
				}
			}
		}
		return $commandee;
	}
	function getArrayPosCodeCommandeeElective($empCode){
		$getTotalPosCode=getPosCodeCommandeeElective($empCode);
		$posCodeArray=explode(",",$getTotalPosCode);
		unset($posCodeArray[sizeof($posCodeArray)-1]);
		return $posCodeArray;
	}
	function getArrayPosCodeCommandee($posCode){
		$getTotalPosCode=getPosCodeCommandee($posCode);
		$posCodeArray=explode(",",$getTotalPosCode);
		unset($posCodeArray[sizeof($posCodeArray)-1]);
		return $posCodeArray;
	}
	
	function getPosCodeCommanderFromDepCode($depCode){
		global $database;
		query("USE hr");
		$getEmpCode=getList("SELECT positionCommand FROM department WHERE depCode = '".$depCode."'");
		return $getEmpCode[0]['positionCommand'];
	}

	function selectionYear($year,$AmountToCover,$id,$style="",$type="B.E.",$var="",$selectedVar=0,$submit=0){
		$var=$var=="" ? date("Y") : $var;
		print("<select name=\"".$id."\" id=\"".$id."\" ".$style." ".($submit ? "onchange=\"this.form.submit();\"" : "").">");
		for($i=$year-$AmountToCover-1;$i<$year+$AmountToCover;$i++){
			if($selectedVar){
				$selected=$i == $var ? "selected=\"selected\"" : "" ;
			}
			switch($type){
				case "B.E."	:	
								print("<option value=\"".$i."\" ".$selected.">".($i+543)."</option>");
								break;
				case "A.D."	:	print("<option value=\"".$i."\" ".$selected.">".$i."</option>");
								break;
			}
			
		}
		print("</select>");
	}

	function selectionMonth($id,$style,$var="",$selectedVar=0,$submit=0){
		global $thaiMonth;
		$var=$var=="" ? date("m") : $var;
		print("<select name=\"".$id."\" id=\"".$id."\" ".$style." ".($submit ? "onchange=\"this.form.submit();\"" : "").">");
		for($index=0;$index<sizeof($thaiMonth);$index++){
			if($selectedVar){
				$selected=($index+1 == $var) ? "selected=\"selected\"" : "" ;
			}
			//print("<option value=\"".str_pad($index+1,2,0,STR_PAD_LEFT)."\" ".$selected.">".$thaiMonth[$index]."</option>");
			print("<option value=\"".($index+1)."\" ".$selected.">".$thaiMonth[$index]."</option>");
		}
		print("</select>");
	}

	function selectionAmount($from,$to,$id,$style,$var="",$firstOption="กรุณาเลือก",$selectedVar=0,$submit=0){
		print("<select name=\"".$id."\" id=\"".$id."\" style=\"".$style."\" ".($submit ? "onchange=\"this.form.submit();\"" : "").">");
		print("<option value=\"-\">".$firstOption."</option>");
		for($index=$from;$index<=$to;$index++){
			if($selectedVar){
				$selected=($index == $var) ? "selected=\"selected\"" : "" ;
			}
			print("<option value=\"".$index."\" ".$selected.">".$index."</option>");
		}
		print("</select>");
	}

	function printHRdataTime($t,$period=8) {
		$min=str_pad(($t%60),2,0,STR_PAD_LEFT);
		$hourt=floor($t/60);
		$hour=str_pad($hourt%$period,($period > 10 ? 2 : 1),0,STR_PAD_LEFT);
		$day=str_pad(floor($hourt/$period),1,0);
		return $day.".".$hour.".".$min;
	}

	function justToVacation($empCode,$option=0){
		/* $option คือชนิดการลาโดยกำหนดดังนี้
			1 คือ การลากิจซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 6 วัน หรือ 2880 นาที
			2 คือ ลาพักร้อนซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 6 วัน หรือ 2880 นาที
			3 คือ ลาคลอดซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 90 วัน หรือ 43200 นาที
			4 คือ ลาทหารซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 60 วัน หรือ 28800 นาที
			5 คือ ลาบวชซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 30 วัน หรือ 14400 นาที*/
		$getDataEmployee=getList("SELECT employee.startDate , employee.stuffDate FROM employee WHERE employee.empCode LIKE '$empCode'");
		$numRateVacation=array(1=>6,2=>6,3=>90,4=>60,5=>30);
		//return $getDataEmployee[0][stuffDate] == 0 ? 0 : $numRateVacation[$option]*8*60;
		
		if(empty($getDataEmployee[0][stuffDate])){
			return 0;
		} else {
			if(date("Y",date2sec($getDataEmployee[0][startDate])) == date("Y") AND date("Y",date2sec($getDataEmployee[0][stuffDate])) == date("Y")){
				return 8*60*(12 - date("m",date2sec($getDataEmployee[0][startDate])) + (date("d",date2sec($getDataEmployee[0][startDate])) <=10 ? 1 : 0))/2;
			} else {
				return $numRateVacation[$option]*8*60;
			}
		}
	}
	function sumOfVacationSpec($empCode,$dateToCheck,$option=0){
		/* $option คือชนิดการลาโดยกำหนดดังนี้
			1 คือ การลากิจซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 6 วัน หรือ 2880 นาที
			2 คือ ลาพักร้อนซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 6 วัน หรือ 2880 นาที
			3 คือ ลาคลอดซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 90 วัน หรือ 43200 นาที
			4 คือ ลาทหารซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 60 วัน หรือ 28800 นาที
			5 คือ ลาบวชซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 30 วัน หรือ 14400 นาที*/
		global $from;
		$getDataEmployee=getList("SELECT employee.stuffDate FROM employee WHERE employee.empCode LIKE '$empCode'");
		list($yearCheck,$monthCheck,$dateCheck)=explode("-",$dateToCheck);
		$vacationArray=array(1=>"1,2",2=>"5,6",3=>"19,20",4=>"21,22",5=>"23,24");
		$type=$vacationArray[$option];
		$getSumVacationOfOption=getList("SELECT SUM(numberTime) AS sum FROM vacation WHERE YEAR(vacationDate)='$yearCheck' AND vacationDate >= '".$getDataEmployee[0][stuffDate]."' AND vacationDate BETWEEN '$from' AND '$dateToCheck' AND vacationTypeID IN ($type) AND empCode LIKE '$empCode'");
		return $getSumVacationOfOption[0][sum]*60;
	}
	
	function sumOfVacation($empCode,$dateToCheck,$option=0){
		/* $option คือชนิดการลาโดยกำหนดดังนี้
			1 คือ การลากิจซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 6 วัน หรือ 2880 นาที
			2 คือ ลาพักร้อนซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 6 วัน หรือ 2880 นาที
			3 คือ ลาคลอดซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 90 วัน หรือ 43200 นาที
			4 คือ ลาทหารซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 60 วัน หรือ 28800 นาที
			5 คือ ลาบวชซึ่งจะกำหนดไว้ 1 ปี สามารถลาได้ 30 วัน หรือ 14400 นาที*/
		$getDataEmployee=getList("SELECT employee.stuffDate FROM employee WHERE employee.empCode LIKE '$empCode'");
		list($yearCheck,$monthCheck,$dateCheck)=explode("-",$dateToCheck);
		$vacationArray=array(1=>"1,2",2=>"5,6",3=>"19,20",4=>"21,22",5=>"23,24");
		$type=$vacationArray[$option];
		$getSumVacationOfOption=getList("SELECT SUM(numberTime) AS sum FROM vacation WHERE YEAR(vacationDate)='$yearCheck' AND vacationDate >= '".$getDataEmployee[0][stuffDate]."' AND vacationDate <= '$dateToCheck' AND vacationTypeID IN ($type) AND empCode LIKE '$empCode'");
		return $getSumVacationOfOption[0][sum]*60;
	}

	function getDepCodeFromEmpCode($empCode){
		query("USE hr");
		$getEmpCode=getList("SELECT depID FROM employee WHERE empCode = '".$empCode."'");
		return $getEmpCode[0][depID];
	}

	function getDepNameFromDepCode($depCode){
		query("USE hr");
		$getEmpCode=getList("SELECT depName FROM department WHERE depCode = '".$depCode."'");
		return $getEmpCode[0][depName];
	}

	function getNameFromEmpCode($empCode){
		query("USE hr");
		$getEmpCode=getLIst("SELECT people.fName , people.sName FROM employee JOIN people ON employee.peopleID = people.peopleID WHERE empCode = '".$empCode."'");
		return $getEmpCode[0]['fName']." ".$getEmpCode[0]['sName'];
	}

	function getPosCodeFromEmpCode($empCode){
		query("USE hr");
		$getEmpCode=getLIst("SELECT position.posCode FROM employee JOIN position ON employee.posCode = position.posCode WHERE employee.empCode = '".$empCode."'");
		return $getEmpCode[0][posCode];
	}

	function getPosNameFromEmpCode($empCode){
		query("USE hr");
		$getEmpCode=getLIst("SELECT position.posName FROM employee JOIN position ON employee.posCode = position.posCode WHERE employee.empCode = '".$empCode."'");
		return $getEmpCode[0][posName];
	}

	function getPosNameFromPosCode($posCode){
		query("USE hr");
		$getEmpCode=getLIst("SELECT position.posName FROM position WHERE position.posCode = '".$posCode."'");
		return $getEmpCode[0][posName];
	}
	
	function getPosCodeFromPosName($posName){
		query("USE hr");
		$getEmpCode=getLIst("SELECT position.posCode FROM position WHERE position.posName = '".$posName."'");
		return $getEmpCode[0][posCode];
	}

	function getHolidayNameFromDate($date){
		query("USE hr");
		$getHolidayName=getLIst("SELECT holidayDetail FROM holiday WHERE holidayDate = '".$date."'");
		return $getHolidayName[0][holidayDetail];
	}

	function productionnewResignUpdate(){
		query("USE hr");
		$getEmpResign=getList("SELECT empCode FROM employee WHERE resignDate = '".date("Y-m-d",mktime(0,0,0,date("m")-2,date("d"),date("Y")))."'");
		for($i=0;$i<sizeof($getEmpResign);$i++){
			list($fName,$sName)=explode(" ",getNameFromEmpCode($getEmpResign[$i][empCode]));
			query("USE productionnew");
			$getData=getList("SELECT * FROM employee WHERE FName = '".$fName."' AND LName = '".$sName."' AND DepartID NOT LIKE '%x%'");
			if(!empty($getData)){
				query_transaction("USE productionnew","UPDATE employee SET DepartID = '".$getData[0][DepartID]."x' WHERE EmployeeID = '".$getData[0][EmployeeID]."'");
			}
		}
	}

	function ArrayDotArray($arr1,$arr2){
		$sumValue=0;
		for($i=0;$i<sizeof($arr1);$i++){
			$sumValue+=$arr1[$i]*$arr2[$i];
		}
		return $sumValue;
	}

	function getNextSubtitle($subtitleID){
		query("USE evaluate");
		$getSubtitle=getList("SELECT subtitleID FROM subtitle WHERE subOf = '".$subtitleID."' AND subtitleIsUsage = '1'");
		$commandee="";
		for($i=0;$i<sizeof($getSubtitle);$i++){
			$commandee.=$getSubtitle[$i][subtitleID].",";
			$tmp=getNextSubtitle($getSubtitle[$i][subtitleID]);
			if(!empty($tmp)){
				$commandee.=$tmp;
			}
		}
		return $commandee;
	}

	function getArrayNextSubtitle($subtitleID){
		$getTotalPosCode=getNextSubtitle($subtitleID);
		$posCodeArray=explode(",",$getTotalPosCode);
		unset($posCodeArray[sizeof($posCodeArray)-1]);
		return $posCodeArray;
	}

	function findDateWork($date,$type="+",$amount){
		$j=0;
		$count=0;
		$numDateNonWorking=0;
		$holiday=getHoliday();
		switch($type){
			case "-"	:
				while($j <= $amount){
					if(!data_in_array(date("Y-m-d",date2sec($date)-($count*60*60*24)),$holiday) AND date("w",date2sec($date)-($count*60*60*24)) != 0){
						$j++;
					} else {
						$numDateNonWorking++;
					}
					$count++;
				}
				return date("Y-m-d",date2sec($date)-(24*60*60*($amount+$numDateNonWorking)));
				break;
			case "+"	:
				while($j <= $amount){
					if(!data_in_array(date("Y-m-d",date2sec($date)+($count*60*60*24)),$holiday) AND date("w",date2sec($date)+($count*60*60*24)) != 0){
						$j++;
					} else {
						$numDateNonWorking++;
					}
					$count++;
				}
				//print(date("Y-m-d",date2sec($date)+(24*60*60*($amount+$numDateNonWorking)))." ".$amount." ".$numDateNonWorking."<br />");
				return date("Y-m-d",date2sec($date)+(24*60*60*($amount+$numDateNonWorking)));
				break;
		}
	}


	

	
	function ReadNumber($number)
	{
		$position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
		$number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
		$number = $number + 0;
		$ret = "";
		if ($number == 0) return $ret;
		if ($number >= 1000000)
		{
			$ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
			$number = intval(fmod($number, 1000000));
		}
		
		$divider = 100000;
		$pos = 0;
		while($number > 0)
		{
			$d = intval($number / $divider);
			$ret .= (($divider == 10) && ($d == 2)) ? "ยี่" : 
				((($divider == 10) && ($d == 1)) ? "" :
				((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
			$ret .= ($d ? $position_call[$pos] : "");
			$number = $number % $divider;
			$divider = $divider / 10;
			$pos++;
		}
		return $ret;
	}
	//ฟังก์ชันปัดเศษทศนิยมแค่ 2 ตำแหน่งไม่สนใจหลักคณิตศาสตร์
	 function utf8_strlen($s) {//นับจำนวนตัวอักษร
		 $c = strlen($s); $l = 0;
		 for ($i = 0; $i < $c; ++$i)
		 if ((ord($s[$i]) & 0xC0) != 0x80) ++$l;
		 return $l;
		 }
	function getMBStrSplit($string, $split_length = 1){
	mb_internal_encoding('UTF-8');
	mb_regex_encoding('UTF-8'); 
	
	$split_length = ($split_length <= 0) ? 1 : $split_length;
	$mb_strlen = mb_strlen($string, 'utf-8');
	$array = array();
	$i = 0; 
	
	while($i < $mb_strlen)
	{
		$array[] = mb_substr($string, $i, $split_length);
		$i = $i+$split_length;
	}
	
	return $array;
}

 function add_date($givendate,$day=0,$mth=0,$yr=0) {//บวกวันที่
        $cd = strtotime($givendate);
        $newdate = date('Y-m-d h:i:s', mktime(date('h',$cd),
        date('i',$cd), date('s',$cd), date('m',$cd)+$mth,
        date('d',$cd)+$day, date('Y',$cd)+$yr));
        return $newdate;
     }
 function cut_date($givendate,$day=0,$mth=0,$yr=0) {//บวกวันที่
        $cd = strtotime($givendate);
        $newdate = date('Y-m-d h:i:s', mktime(date('h',$cd),
        date('i',$cd), date('s',$cd), date('m',$cd)-$mth,
        date('d',$cd)-$day, date('Y',$cd)-$yr));
        return $newdate;
     }
?>